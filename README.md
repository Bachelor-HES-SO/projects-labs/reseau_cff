# Réseau CFF

> version Java : JDK 1.8

![]()

![img1](./docs/img1.png)

## Parcours entre deux villes

![](./docs/img2.png)

## Matrice des temps de parcours (Floyd)

![](./docs/img3.png)

## Matrice des précédences (Floyd)

![](./docs/img4.png)

## Tableau de parcours de Dijkstra

![](./docs/img5.png)